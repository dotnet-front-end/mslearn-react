import React from 'react';

// TODO: Create RecipeTitle component
function RecipeTitle(props) {
    return (
        <section>
            <h2>{ props.title }</h2>
        </section>
    )
};

/*function RecipeTitle() {
    const title = 'Mashed potatoes';
    return (
        <h2>{ title }</h2>
    )
};*/
export default RecipeTitle;