import React from 'react';

const numbers = [2, 5, 8];
const squared = numbers.map((number) => {
  return number * number;
});

const squaredi = numbers.map((number, index) => {
  console.log(`Processing item ${index + 1}`);
  return number * number;
});

console.log('squared', squared);
// Output: [4, 25, 64]

console.log('squaredi', squaredi);
// Output: [4, 25, 64]

function App() {
  return (
    <article>
      <h1>Use <i>map</i>!</h1>
      <h2>Map com React</h2>
    </article>
    )
}

export default App;