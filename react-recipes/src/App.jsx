import React from 'react';
import RecipeTitle from './RecipeTitle';
import './index.css';
import logo from './img/logo.png'
// TODO: Import IngredientList
import IngredientList from './IngredientList'

// TODO: Add recipe object
const recipe = {
    title: 'Mashed potatoes',
    feedback: {
        rating: 4.8,
        reviews: 20
    },
    ingredients: [
        { name: '3 potatoes, cut into 1/2" pieces', prepared: false },
        { name: '4 Tbsp butter', prepared: false },
        { name: '1/8 cup heavy cream', prepared: false },
        { name: 'Salt', prepared: true },
        { name: 'Pepper', prepared: true },
    ],
    phases: [
        { todo: 'Adicione as batatas cortadas a uma panela com água e sal', prepared: 1 },
        { todo: 'Leve a panela para ferver', prepared: 1 },
        { todo: 'Ferva as batatas até ficarem macias, por cerca de 15 a 20 minutos', prepared: 1 },
        { todo: 'Coe as batatas', prepared: 1 },
        { todo: 'Coloque-as novamente na panela', prepared: 1 },
        { todo: 'Adicione a manteiga, o creme de leite, o sal e a pimenta a gosto', prepared: 1 },
        { todo: 'Amasse as batatas', prepared: 1 },
        { todo: 'Tempere novamente e adicione a manteiga e o creme de leite conforme desejado', prepared: 1 },
    ],
};

function App() {
    return (
        <article>
            <img src={logo} alt="logo" />
            <h1>Recipe Manager</h1>
            {/* TODO: Add RecipeTitle component */}
            <RecipeTitle title={ recipe.title } feedback={ recipe.feedback } />
            {/* TODO: Add IngredientList component */}
            <IngredientList ingredients={recipe.ingredients} />
        </article>
    )
}

export default App;