**Criar um projeto do React do zero**

Bash
````
# Windows
md react-recipes && cd react-recipes
md src
md public
touch package.json
echo "{}" > package.json
# Linux or macOS
mkdir react-recipes && cd react-recipes
mkdir src
mkdir public
touch package.json
echo "{}" > package.json
````

Execute o código a seguir no mesmo terminal ou janela de comando.

Bash
````
npm install --save-dev snowpack
npm install react react-dom
````

Observação

O Snowpack é uma dependência de desenvolvimento. Ou seja, ele não é necessário para a produção, pois ele gera os arquivos JavaScript e HTML requeridos durante o processo de compilação.

Abra o diretório no Visual Studio Code executando o comando a seguir.

Bash
````
code .
````

