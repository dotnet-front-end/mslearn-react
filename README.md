**Introdução ao React**
reference: [react-get-started](https://docs.microsoft.com/pt-br/learn/modules/react-get-started/)

 1. CLASSIFICADO

   - Iniciante
   - Estudante
   - Desenvolvedor
   - Aplicativos Web Estáticos do Serviço de Aplicativo
   - Visual Studio Code

O React.js é a estrutura JavaScript de front-end mais popular. Os desenvolvedores usam o JSX, uma combinação de HTML e JavaScript, para criar exibições de forma natural. Eles também podem criar componentes para blocos que podem ser reutilizados em seus aplicativos. Este módulo apresenta o React e as principais habilidades de que os desenvolvedores precisam para usar essa poderosa estrutura

  2. Objetivos de aprendizagem

Neste módulo, você vai:

   - Saber como definir o JSX
   - Instalar o React
   - Criar um projeto do React
   - Criar um componente do React
   - Trabalhar com dados dinâmicos
   - Trabalhar com CSS

  3. Pré-requisitos

   - Conhecimento de HTML, CSS, JavaScript e Git
   - Conhecimento do gerenciamento de pacotes com Node.js e npm
   - Um editor de códigos, como o Visual Studio Code

  4. Este módulo faz parte destes roteiros de aprendizagem

Crie seus primeiros aplicativos Web com o React

   - Introdução ao React
   - Apresentação ao JSX
   - Criar um projeto inicial
   - Hello, world!
   - Criar seu primeiro componente
   - Exibir dados dinâmicos
   - Adicionar estilo
   - Criar um projeto do React do zero
   - Verificação de conhecimentos
   - Resumo

© Microsoft 2021